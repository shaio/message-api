import hug
from telegram.ext import Updater, CommandHandler
import config

updater = Updater(token=config.token, use_context=True)
dispatcher = updater.dispatcher

def get_chat_id(update, context):
    current_chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=current_chat_id, text=current_chat_id)


chat_id_handler = CommandHandler('getchatid', get_chat_id)
dispatcher.add_handler(chat_id_handler)

@hug.post('/message')
def message(body):
    text_to_send = body.get('message', '')
    if text_to_send == '' or text_to_send is None:
        return 'not done'

    bot = updater.bot
    bot.sendMessage(chat_id=config.chat_id, text=text_to_send)
    
    return 'done'


updater.start_polling()
