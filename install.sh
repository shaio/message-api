#/bin/bash
PORT=8090
VIRTUALENV=$HOME/.virtualenvs/messages-api
CURRENTFOLDER=$(pwd)
USER=$USER

if ! test -f "config.py"; then
  echo "No existe el archivo config.py en el directorio."
  exit 1
fi

if grep "^chat_id =$" config.py; then
  echo "No se ha establecido un chat_id, al iniciar el programa es recomendable obtenerlo con /getchatid, establecerlo en config.py, y reiniciar el servicio"
fi

if grep "^token = ''$" config.py; then
  echo "Por favor, establezca un token válido para iniciar."
  exit 1
fi

sudo apt install python3 python3-pip &&
sudo python3 -m pip install virtualenv &&
mkdir -p $HOME/.virtualenvs &&
virtualenv $HOME/.virtualenvs/messages-api &&
source $HOME/.virtualenvs/messages-api/bin/activate &&
pip install -r requirements.txt &&

sudo rm -f message-api-bot.service
cp plantilla.service message-api-bot.service
echo $VIRTUALENV
sed -i "s|{VIRTUALENV}|${VIRTUALENV}|g" message-api-bot.service
sed -i "s|{PORT}|${PORT}|g" message-api-bot.service
sed -i "s|{CURRENTFOLDER}|${CURRENTFOLDER}|g" message-api-bot.service
sed -i "s|{USER}|${USER}|g" message-api-bot.service

sudo mv message-api-bot.service /etc/systemd/system/message-api-bot.service

echo "DONE!!"
echo "Por favor habilitar el servicio con "
echo "systemctl start message-api-bot.service"

deactivate
